'use strict';

const path = require('path');
const OAuth2 = require('./lib/OAuth2');

module.exports = app => {
  app.coreLogger.info('[egg-oauth2] egg-oauth2 begin start');

  const start = Date.now();

  const config = app.config.oauth2;

  const Model = app.loader.loadFile(path.join(app.config.baseDir, config.model));

  if(Model === null) {
    app.coreLogger.error('[egg-oauth2] not find "%s", egg-oauth2 start fail', config.model);

    return;
  }

  try {
    app.oauth2 = new OAuth2(Model, config);
  }catch(e) {
    app.coreLogger.error('[egg-oauth2] start fail, %s', e);

    return;
  }

  app.coreLogger.info('[egg-oauth2] egg-oauth2 started use %d ms', Date.now() - start);
};
