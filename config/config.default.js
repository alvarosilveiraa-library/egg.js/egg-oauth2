'use strict';

module.exports = {
  oauth2: {
    debug: true,
    grants: [
      'password'
    ],
    model: 'app/extend/oauth2.js'
  }
};
