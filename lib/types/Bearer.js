'use strict';

const ServerError = require('../ServerError');

class BearerType {
  constructor(accessToken, accessTokenLifetime, refreshToken, scope) {
    if(!accessToken)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `accessToken`'
      });

    this.accessToken = accessToken;

    this.accessTokenLifetime = accessTokenLifetime;

    this.refreshToken = refreshToken;

    this.scope = scope;
  }

  valueOf() {
    return {
      access_token: this.accessToken,
      token_type: 'Bearer',
      expires_in: this.accessTokenLifetime,
      refresh_token: this.refreshToken,
      scope: this.scope
    };
  }
}

module.exports = BearerType;
