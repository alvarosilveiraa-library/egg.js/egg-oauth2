'use strict';

const ServerError = require('../ServerError');

class CodeType {
  constructor(code, state) {
    if(!code || !code.authorizationCode)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `code`'
      });

    this.code = code;

    this.state = state;
  }

  valueOf() {
    return {
      ...this.code,
      state: this.state
    };
  }
}

module.exports = CodeType;
