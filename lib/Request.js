'use strict';

const typeis = require('type-is');
const ServerError = require('./ServerError');

class Request {
  constructor(options = {}) {
    if(!options.method)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `method`'
      });

    this.body = options.body || {};

    this.headers = {};

    this.method = options.method;

    this.query = options.query;

    for(const field in options.headers)
      if(options.headers.hasOwnProperty(field))
        this.headers[field.toLowerCase()] = options.headers[field];

    for(const property in options)
      if(options.hasOwnProperty(property) && !this[property])
        this[property] = options[property];
  }

  get(field) {
    return this.headers[field.toLowerCase()];
  }

  is(types) {
    if(!Array.isArray(types))
      types = [].slice.call(arguments);

    return typeis(this, types) || false;
  }
}

module.exports = Request;
