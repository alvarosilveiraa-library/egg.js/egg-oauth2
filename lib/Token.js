'use strict';

const _ = require('lodash');
const auth = require('basic-auth');
const Promise = require('bluebird');
const promisify = require('promisify-any').use(Promise);
const Request = require('./Request');
const Response = require('./Response');
const ServerError = require('./ServerError');
const TokenModel = require('./models/Token');
const BearerType = require('./types/Bearer');
const {
  is
} = require('./util');

const GRANT_TYPES = {
  authorization_code: require('./grant-types/AuthorizationCode'),
  client_credentials: require('./grant-types/ClientCredentials'),
  password: require('./grant-types/Password'),
  refresh_token: require('./grant-types/RefreshToken')
};

class Token {
  constructor(options = {}) {
    if(!options.accessTokenLifetime)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `accessTokenLifetime`'
      });

    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.refreshTokenLifetime)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `refreshTokenLifetime`'
      });

    if(!options.model.getClient)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getClient()`'
      });

    this.model = options.model;

    this.accessTokenLifetime = options.accessTokenLifetime;

    this.refreshTokenLifetime = options.refreshTokenLifetime;

    this.allowExtendedTokenAttributes = options.allowExtendedTokenAttributes;

    this.requireClientAuthentication = options.requireClientAuthentication || {};

    this.alwaysIssueNewRefreshToken = options.alwaysIssueNewRefreshToken || false;
  }

  handle(request, response) {
    if(!(request instanceof Request))
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: `request` must be an instance of Request'
      });

    if(!(response instanceof Response))
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: `response` must be an instance of Response'
      });

    if(request.method !== 'POST')
      throw new ServerError({
        status: 400,
        message: 'Invalid request: method must be POST'
      });

    if(!request.is('application/x-www-form-urlencoded'))
      return Promise.reject({
        status: 400,
        message: 'Invalid request: content must be application/x-www-form-urlencoded'
      });

    return Promise.bind(this)
      .then(() => this.getClient(request, response))
      .then(client => this.handleGrantType(request, client))
      .tap(data => {
        const model = new TokenModel(data, {
          allowExtendedTokenAttributes: this.allowExtendedTokenAttributes
        });

        const tokenType = this.getTokenType(model);

        this.updateSuccessResponse(response, tokenType);
      });
  }

  getClient(request, response) {
    const credentials = this.getClientCredentials(request);

    const grantType = request.body.grant_type;

    if(!credentials.clientId)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `client_id`'
      });

    if(this.isClientAuthenticationRequired(grantType) && !credentials.clientSecret)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `client_secret`'
      });

    if(!is.vschar(credentials.clientId))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `client_id`'
      });

    if(credentials.clientSecret && !is.vschar(credentials.clientSecret))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `client_secret`'
      });

    return promisify(this.model.getClient, 2).call(this.model, credentials.clientId, credentials.clientSecret)
      .then(function(client) {
        if(!client)
          throw new ServerError({
            status: 400,
            message: 'Invalid client: client is invalid'
          });

        if(!client.grants)
          throw new ServerError({
            status: 500,
            message: 'Server error: missing client `grants`'
          });

        if(!(client.grants instanceof Array))
          throw new ServerError({
            status: 500,
            message: 'Server error: `grants` must be an array'
          });

        return client;
      })
      .catch(e => {
        if(e.status === 400 && request.get('authorization'))
          response.set('WWW-Authenticate', 'Basic realm="Service"');

        throw e;
      });
  }

  getClientCredentials(request) {
    const credentials = auth(request);

    const grantType = request.body.grant_type;

    if(credentials)
      return {
        clientId: credentials.name,
        clientSecret: credentials.pass
      };

    if(request.body.client_id && request.body.client_secret)
      return {
        clientId: request.body.client_id,
        clientSecret: request.body.client_secret
      };

    if(!this.isClientAuthenticationRequired(grantType) && request.body.client_id)
      return {
        clientId: request.body.client_id
      };

    throw new ServerError({
      status: 400,
      message: 'Invalid client: cannot retrieve client credentials'
    });
  }

  handleGrantType(request, client) {
    const grantType = request.body.grant_type;

    if(!grantType)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `grant_type`'
      });

    if(!is.nchar(grantType) && !is.uri(grantType))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `grant_type`'
      });

    if(!_.has(GRANT_TYPES, grantType))
      throw new ServerError({
        status: 400,
        message: 'Unsupported grant type: `grant_type` is invalid'
      });

    if(!_.includes(client.grants, grantType))
      throw new ServerError({
        status: 400,
        message: 'Unauthorized client: `grant_type` is invalid'
      });

    const accessTokenLifetime = this.getAccessTokenLifetime(client);

    const refreshTokenLifetime = this.getRefreshTokenLifetime(client);

    const GrantType = GRANT_TYPES[grantType];

    return new GrantType({
      accessTokenLifetime: accessTokenLifetime,
      model: this.model,
      refreshTokenLifetime: refreshTokenLifetime,
      alwaysIssueNewRefreshToken: this.alwaysIssueNewRefreshToken
    }).handle(request, client);
  }

  getAccessTokenLifetime(client) {
    return client.accessTokenLifetime || this.accessTokenLifetime;
  }

  getRefreshTokenLifetime(client) {
    return client.refreshTokenLifetime || this.refreshTokenLifetime;
  }

  getTokenType(model) {
    return new BearerType(model.accessToken, model.accessTokenLifetime, model.refreshToken, model.scope);
  }

  updateSuccessResponse(response, tokenType) {
    response.body = tokenType.valueOf();

    response.set('Cache-Control', 'no-store');

    response.set('Pragma', 'no-cache');
  }

  isClientAuthenticationRequired(grantType) {
    if(Object.keys(this.requireClientAuthentication).length > 0)
      return (typeof this.requireClientAuthentication[grantType] !== 'undefined')
        ? this.requireClientAuthentication[grantType]
        : true;
    else return true;
  }
}

module.exports = Token;
