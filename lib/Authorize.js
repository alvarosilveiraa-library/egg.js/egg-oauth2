'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const promisify = require('promisify-any').use(Promise);
const Request = require('./Request');
const Response = require('./Response');
const Authenticate = require('./Authenticate');
const CodeType = require('./types/Code');
const ServerError = require('./ServerError');
const {
  is,
  generateRandomToken
} = require('./util');

class Authorize {
  constructor(options = {}) {
    if(options.authenticateHandler && !options.authenticateHandler.handle)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: authenticateHandler does not implement `handle()`'
      });

    if(!options.authorizationCodeLifetime)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `authorizationCodeLifetime`'
      });

    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.model.getClient)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getClient()`'
      });

    if(!options.model.saveAuthorizationCode)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `saveAuthorizationCode()`'
      });

    this.model = options.model;

    this.allowEmptyState = options.allowEmptyState;

    this.authenticateHandler = options.Authenticate
      ? new options.Authenticate(options)
      : new Authenticate(options);

    this.authorizationCodeLifetime = options.authorizationCodeLifetime;
  }

  handle(request, response) {
    if(!(request instanceof Request))
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: `request` must be an instance of Request'
      });

    if(!(response instanceof Response))
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: `response` must be an instance of Response'
      });

    if(request.query.allowed === 'false')
      throw new ServerError({
        status: 400,
        message: 'Access denied: user denied access to application'
      });

    const fns = [
      this.getAuthorizationCodeLifetime(),
      this.getClient(request),
      this.getUser(request, response)
    ];

    return Promise.all(fns)
      .bind(this)
      .spread((expiresAt, client, user) => {
        const scope = this.getScope(request);

        const state = this.getState(request);

        const uri = this.getRedirectUri(request);

        return Promise.bind(this)
          .then(() => this.generateAuthorizationCode(client, user, scope))
          .then(authorizationCode => this.saveAuthorizationCode(authorizationCode, expiresAt, scope, client, uri, user))
          .then(code => {
            const responseType = this.getResponseType(request, code, state);

            this.updateSuccessResponse(response, responseType);

            return code;
          });
      });
  }

  generateAuthorizationCode(client, user, scope) {
    if(this.model.generateAuthorizationCode)
      return promisify(this.model.generateAuthorizationCode).call(this.model, client, user, scope);

    return generateRandomToken();
  }

  getAuthorizationCodeLifetime() {
    const expires = new Date();

    expires.setSeconds(expires.getSeconds() + this.authorizationCodeLifetime);

    return expires;
  }

  getClient(request) {
    const clientId = request.body.client_id || request.query.client_id;

    if(!clientId)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `client_id`'
      });

    if(!is.vschar(clientId))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `client_id`'
      });

    const redirectUri = request.body.redirect_uri || request.query.redirect_uri;

    if(redirectUri && !is.uri(redirectUri))
      throw new ServerError({
        status: 400,
        message: 'Invalid request: `redirect_uri` is not a valid URI'
      });

    return promisify(this.model.getClient, 2).call(this.model, clientId, null)
      .then(client => {
        if(!client)
          throw new ServerError({
            status: 400,
            message: 'Invalid client: client credentials are invalid'
          });

        if(!client.grants)
          throw new ServerError({
            status: 400,
            message: 'Invalid client: missing client `grants`'
          });

        if(!_.includes(client.grants, 'authorization_code'))
          throw new ServerError({
            status: 400,
            message: 'Unauthorized client: `grant_type` is invalid'
          });

        return client;
      });
  }

  getScope(request) {
    const scope = request.body.scope || request.query.scope;

    if(!is.nqschar(scope))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `scope`'
      });

    return scope;
  }

  getState(request) {
    const state = request.body.state || request.query.state;

    if(!this.allowEmptyState && !state)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `state`'
      });

    if(!is.vschar(state))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `state`'
      });

    return state;
  }

  getUser(request, response) {
    if(this.authenticateHandler instanceof Authenticate)
      return this.authenticateHandler.handle(request, response).get('user');

    return this.authenticateHandler.handle(request, response)
      .then(user => {
        if(!user)
          throw new ServerError({
            status: 500,
            message: 'Server error: `handle()` did not return a `user` object'
          });

        return user;
      });
  }

  getRedirectUri(request) {
    return request.body.redirect_uri || request.query.redirect_uri;
  }

  saveAuthorizationCode(authorizationCode, expiresAt, scope, client, redirectUri, user) {
    const code = {
      authorizationCode,
      expiresAt,
      redirectUri,
      scope
    };

    return promisify(this.model.saveAuthorizationCode, 3).call(this.model, code, client, user);
  }

  getResponseType(request, code, state) {
    const responseType = request.body.response_type || request.query.response_type;

    if(!responseType)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `response_type`'
      });

    switch(responseType) {
      case 'code':
        return new CodeType(code, state);
      default:
        throw new ServerError({
          status: 400,
          message: 'Unsupported response type: `response_type` is not supported'
        });
    }
  }

  buildSuccessRedirectUri(redirectUri, responseType) {
    return responseType.buildRedirectUri(redirectUri);
  }

  updateSuccessResponse(response, responseType) {
    response.body = responseType.valueOf();

    response.set('Cache-Control', 'no-store');

    response.set('Pragma', 'no-cache');
  }
}

module.exports = Authorize;
