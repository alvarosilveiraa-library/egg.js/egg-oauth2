'use strict';

const Promise = require('bluebird');
const promisify = require('promisify-any').use(Promise);
const AbstractGrantType = require('./Abstract');
const ServerError = require('../ServerError');
const {
  is
} = require('../util');

class PasswordGrantType extends AbstractGrantType {
  constructor(options = {}) {
    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.model.getUser)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getUser()`'
      });

    if(!options.model.saveToken)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `saveToken()`'
      });

    super(options);
  }

  handle(request, client) {
    if(!request)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `request`'
      });

    if(!client)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `client`'
      });

    const scope = this.getScope(request);

    return Promise.bind(this)
      .then(() => this.getUser(request))
      .then(user => this.saveToken(user, client, scope));
  }

  getUser(request) {
    if(!request.body.username)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `username`'
      });

    if(!request.body.password)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `password`'
      });

    if(!is.uchar(request.body.username))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `username`'
      });

    if(!is.uchar(request.body.password))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `password`'
      });

    return promisify(this.model.getUser, 2).call(this.model, request.body.username, request.body.password)
      .then(user => {
        if(!user)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: user credentials are invalid'
          });

        return user;
      });
  }

  saveToken(user, client, scope) {
    const fns = [
      this.validateScope(user, client, scope),
      this.generateAccessToken(client, user, scope),
      this.generateRefreshToken(client, user, scope),
      this.getAccessTokenExpiresAt(),
      this.getRefreshTokenExpiresAt()
    ];

    return Promise.all(fns).bind(this)
      .spread((scope, accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt) => {
        const token = {
          accessToken: accessToken,
          accessTokenExpiresAt: accessTokenExpiresAt,
          refreshToken: refreshToken,
          refreshTokenExpiresAt: refreshTokenExpiresAt,
          scope: scope
        };

        return promisify(this.model.saveToken, 3).call(this.model, token, client, user);
      });
  }
}

module.exports = PasswordGrantType;
