'use strict';

const Promise = require('bluebird');
const promisify = require('promisify-any').use(Promise);
const AbstractGrantType = require('./Abstract');
const ServerError = require('../ServerError');
const {
  is
} = require('../util');

class RefreshTokenGrantType extends AbstractGrantType {
  constructor(options = {}) {
    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.model.getRefreshToken)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getRefreshToken()`'
      });

    if(!options.model.revokeToken)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `revokeToken()`'
      });

    if(!options.model.saveToken)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `saveToken()`'
      });

    super(options);
  }

  handle(request, client) {
    if(!request)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `request`'
      });

    if(!client)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `client`'
      });

    return Promise.bind(this)
      .then(() => this.getRefreshToken(request, client))
      .tap(token => this.revokeToken(token))
      .then(token => this.saveToken(token.user, client, token.scope));
  }

  getRefreshToken(request, client) {
    if(!request.body.refresh_token)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `refresh_token`'
      });

    if(!is.vschar(request.body.refresh_token))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `refresh_token`'
      });

    return promisify(this.model.getRefreshToken, 1).call(this.model, request.body.refresh_token)
      .then(token => {
        if(!token)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: refresh token is invalid'
          });

        if(!token.client)
          throw new ServerError({
            status: 500,
            message: 'Server error: `getRefreshToken()` did not return a `client` object'
          });

        if(!token.user)
          throw new ServerError({
            status: 500,
            message: 'Server error: `getRefreshToken()` did not return a `user` object'
          });

        if(token.client.id !== client.id)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: refresh token is invalid'
          });

        if(token.refreshTokenExpiresAt && !(token.refreshTokenExpiresAt instanceof Date))
          throw new ServerError({
            status: 500,
            message: 'Server error: `refreshTokenExpiresAt` must be a Date instance'
          });

        if(token.refreshTokenExpiresAt && token.refreshTokenExpiresAt < new Date())
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: refresh token has expired'
          });

        return token;
      });
  }

  revokeToken(token) {
    if(this.alwaysIssueNewRefreshToken === false) return Promise.resolve(token);

    return promisify(this.model.revokeToken, 1).call(this.model, token)
      .then(status => {
        if(!status)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: refresh token is invalid'
          });

        return token;
      });
  }

  saveToken(user, client, scope) {
    const fns = [
      this.generateAccessToken(client, user, scope),
      this.generateRefreshToken(client, user, scope),
      this.getAccessTokenExpiresAt(),
      this.getRefreshTokenExpiresAt()
    ];

    return Promise.all(fns).bind(this)
      .spread((accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt) => {
        const token = {
          accessToken: accessToken,
          accessTokenExpiresAt: accessTokenExpiresAt,
          scope: scope
        };

        if(this.alwaysIssueNewRefreshToken !== false) {
          token.refreshToken = refreshToken;
          token.refreshTokenExpiresAt = refreshTokenExpiresAt;
        }

        return token;
      })
      .then(token => promisify(this.model.saveToken, 3).call(this.model, token, client, user));
  }
}

module.exports = RefreshTokenGrantType;
