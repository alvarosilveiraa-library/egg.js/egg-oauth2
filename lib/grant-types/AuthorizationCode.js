'use strict';

const Promise = require('bluebird');
const promisify = require('promisify-any').use(Promise);
const AbstractGrantType = require('./Abstract');
const ServerError = require('../ServerError');
const {
  is
} = require('../util');

class AuthorizationCodeGrantType extends AbstractGrantType {
  constructor(options = {}) {
    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.model.getAuthorizationCode)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getAuthorizationCode()`'
      });

    if(!options.model.revokeAuthorizationCode)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getAuthorizationCode()`'
      });

    if(!options.model.saveToken)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `saveToken()`'
      });

    super(options);
  }

  handle(request, client) {
    if(!request)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `request`'
      });

    if(!client)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `client`'
      });

    return Promise.bind(this)
      .then(() => this.getAuthorizationCode(request, client))
      .tap(code => this.validateRedirectUri(request, code))
      .tap(code => this.revokeAuthorizationCode(code))
      .then(code => this.saveToken(code.user, client, code.authorizationCode, code.scope));
  }

  getAuthorizationCode(request, client) {
    if(!request.body.code)
      throw new ServerError({
        status: 400,
        message: 'Missing parameter: `code`'
      });

    if(!is.vschar(request.body.code))
      throw new ServerError({
        status: 400,
        message: 'Invalid parameter: `code`'
      });

    return promisify(this.model.getAuthorizationCode, 1).call(this.model, request.body.code)
      .then(code => {
        if(!code)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: authorization code is invalid'
          });

        if(!code.client)
          throw new ServerError({
            status: 500,
            message: 'Server error: `getAuthorizationCode()` did not return a `client` object'
          });

        if(!code.user)
          throw new ServerError({
            status: 500,
            message: 'Server error: `getAuthorizationCode()` did not return a `user` object'
          });

        if(code.client.id !== client.id)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: authorization code is invalid'
          });

        if(!(code.expiresAt instanceof Date))
          throw new ServerError({
            status: 500,
            message: 'Server error: `expiresAt` must be a Date instance'
          });

        if(code.expiresAt < new Date())
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: authorization code has expired'
          });

        if(code.redirectUri && !is.uri(code.redirectUri))
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: `redirect_uri` is not a valid URI'
          });

        return code;
      });
  }

  validateRedirectUri(request, code) {
    if(!code.redirectUri) return;

    const redirectUri = request.body.redirect_uri || request.query.redirect_uri;

    if(!is.uri(redirectUri))
      throw new ServerError({
        status: 400,
        message: 'Invalid request: `redirect_uri` is not a valid URI'
      });

    if(redirectUri !== code.redirectUri)
      throw new ServerError({
        status: 400,
        message: 'Invalid request: `redirect_uri` is invalid'
      });
  }

  revokeAuthorizationCode(code) {
    return promisify(this.model.revokeAuthorizationCode, 1).call(this.model, code)
      .then(status => {
        if(!status)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: authorization code is invalid'
          });

        return code;
      });
  }

  saveToken(user, client, authorizationCode, scope) {
    const fns = [
      this.validateScope(user, client, scope),
      this.generateAccessToken(client, user, scope),
      this.generateRefreshToken(client, user, scope),
      this.getAccessTokenExpiresAt(),
      this.getRefreshTokenExpiresAt()
    ];

    return Promise.all(fns).bind(this)
      .spread((scope, accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt) => {
        const token = {
          accessToken: accessToken,
          authorizationCode: authorizationCode,
          accessTokenExpiresAt: accessTokenExpiresAt,
          refreshToken: refreshToken,
          refreshTokenExpiresAt: refreshTokenExpiresAt,
          scope: scope
        };

        return promisify(this.model.saveToken, 3).call(this.model, token, client, user);
      });
  }
}

module.exports = AuthorizationCodeGrantType;
