'use strict';

const Promise = require('bluebird');
const promisify = require('promisify-any').use(Promise);
const AbstractGrantType = require('./Abstract');
const ServerError = require('../ServerError');

class ClientCredentialsGrantType extends AbstractGrantType {
  constructor(options = {}) {
    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.model.getUserFromClient)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getUserFromClient()`'
      });

    if(!options.model.saveToken)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `saveToken()`'
      });

    super(options);
  }

  handle(request, client) {
    if(!request)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `request`'
      });

    if(!client)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `client`'
      });

    const scope = this.getScope(request);

    return Promise.bind(this)
      .then(() => this.getUserFromClient(client))
      .then(user => this.saveToken(user, client, scope));
  }

  getUserFromClient(client) {
    return promisify(this.model.getUserFromClient, 1).call(this.model, client)
      .then(user => {
        if(!user)
          throw new ServerError({
            status: 400,
            message: 'Invalid grant: user credentials are invalid'
          });

        return user;
      });
  }

  saveToken(user, client, scope) {
    const fns = [
      this.validateScope(user, client, scope),
      this.generateAccessToken(client, user, scope),
      this.getAccessTokenExpiresAt(client, user, scope)
    ];

    return Promise.all(fns).bind(this)
      .spread((scope, accessToken, accessTokenExpiresAt) => {
        const token = {
          accessToken: accessToken,
          accessTokenExpiresAt: accessTokenExpiresAt,
          scope: scope
        };

        return promisify(this.model.saveToken, 3).call(this.model, token, client, user);
      });
  }
}

module.exports = ClientCredentialsGrantType;
