'use strict';

const Promise = require('bluebird');
const promisify = require('promisify-any').use(Promise);
const ServerError = require('../ServerError');
const {
  is,
  generateRandomToken
} = require('../util');

class AbstractGrantType {
  constructor(options = {}) {
    if(!options.accessTokenLifetime)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `accessTokenLifetime`'
      });

    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    this.model = options.model;

    this.accessTokenLifetime = options.accessTokenLifetime;

    this.refreshTokenLifetime = options.refreshTokenLifetime;

    this.alwaysIssueNewRefreshToken = options.alwaysIssueNewRefreshToken;
  }

  generateAccessToken(client, user, scope) {
    if(this.model.generateAccessToken)
      return promisify(this.model.generateAccessToken, 3).call(this.model, client, user, {
        scope,
        expiresAt: this.getAccessTokenExpiresAt()
      }).then(accessToken => accessToken || generateRandomToken());

    return generateRandomToken();
  }

  generateRefreshToken(client, user, scope) {
    if(this.model.generateRefreshToken)
      return promisify(this.model.generateRefreshToken, 3).call(this.model, client, user, {
        scope,
        expiresAt: this.getRefreshTokenExpiresAt()
      }).then(refreshToken => refreshToken || generateRandomToken());

    return generateRandomToken();
  }

  getAccessTokenExpiresAt() {
    const expires = new Date();

    expires.setSeconds(expires.getSeconds() + this.accessTokenLifetime);

    return expires;
  }

  getRefreshTokenExpiresAt() {
    const expires = new Date();

    expires.setSeconds(expires.getSeconds() + this.refreshTokenLifetime);

    return expires;
  }

  getScope(request) {
    if(!is.nqschar(request.body.scope))
      throw new ServerError({
        status: 500,
        message: 'Invalid parameter: `scope`'
      });

    return request.body.scope;
  }

  validateScope(user, client, scope) {
    if(this.model.validateScope)
      return promisify(this.model.validateScope, 3).call(this.model, user, client, scope)
        .then(scope => {
          if(!scope)
            throw new ServerError({
              status: 400,
              message: 'Invalid scope: Requested scope is invalid'
            });

          return scope;
        });

    return scope;
  }
}

module.exports = AbstractGrantType;
