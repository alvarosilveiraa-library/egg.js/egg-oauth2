'use strict';

const _ = require('lodash');
const Server = require('./Server');
const Request = require('./Request');
const Response = require('./Response');
const Password = require('./Password');
const ServerError = require('./ServerError');
const {
  replaceResponse,
  handleResponseExpress,
  handleErrorExpress
} = require('./util');

class OAuth2Express {
  constructor(Model, options = {}) {
    if(!Model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `Model`'
      });

    this.Model = Model;

    this.options = options;
  }

  get server() {
    const {
      req,
      res,
      Model,
      options
    } = this;

    const model = new Model(req, res);

    const server = new Server(_.assign(options, { model }));

    return server;
  }

  async execute(handle, req, res, options) {
    let result = null;

    this.req = req;

    this.res = res;

    const request = new Request(req);

    const response = new Response(replaceResponse(req));

    try {
      result = await this.server[handle](request, response, options);

      handleResponseExpress(res, response);
    }catch(e) {
      handleErrorExpress(res, response, e);
    }

    return result;
  }

  token(options) {
    return async (req, res, next) => {
      const response = await this.execute('token', req, res, options);

      req.oauth2 = response;

      if(response) await next();
    };
  }

  authenticate(options) {
    return async (req, res, next) => {
      const response = await this.execute('authenticate', req, res, options);

      req.oauth2 = response;

      if(response) await next();
    };
  }

  authorize(options = {}) {
    return async (req, res, next) => {
      options = _.assign({
        Authenticate: Password
      }, options);

      const response = await this.execute('authorize', req, res, options);

      req.oauth2 = response;

      if(response) await next();
    };
  }
}

module.exports = OAuth2Express;
