'use strict';

const _ = require('lodash');
const Server = require('./Server');
const Request = require('./Request');
const Response = require('./Response');
const Password = require('./Password');
const ServerError = require('./ServerError');
const {
  replaceResponse,
  handleResponse,
  handleError
} = require('./util');

class OAuth2 {
  constructor(Model, options = {}) {
    if(!Model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `Model`'
      });

    this.Model = Model;

    this.options = options;
  }

  get server() {
    const {
      ctx,
      Model,
      options
    } = this;

    const model = new Model(ctx);

    const server = new Server(_.assign(options, { model }));

    return server;
  }

  async execute(handle, ctx, options) {
    let result = null;

    this.ctx = ctx;

    const request = new Request(ctx.request);

    const response = new Response(replaceResponse(ctx.response));

    try {
      result = await this.server[handle](request, response, options);

      handleResponse(ctx, response);
    }catch(e) {
      handleError(ctx, response, e);
    }

    return result;
  }

  token(options) {
    return async (ctx, next) => {
      const response = await this.execute('token', ctx, options);

      ctx.state.oauth2 = response;

      if(response) await next();
    };
  }

  authenticate(options) {
    return async (ctx, next) => {
      const response = await this.execute('authenticate', ctx, options);

      ctx.state.oauth2 = response;

      if(response) await next();
    };
  }

  authorize(options = {}) {
    return async (ctx, next) => {
      options = _.assign({
        Authenticate: Password
      }, options);

      const response = await this.execute('authorize', ctx, options);

      ctx.state.oauth2 = response;

      if(response) await next();
    };
  }
}

module.exports = OAuth2;
