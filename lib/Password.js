'use strict';

const Promise = require('bluebird');
const Request = require('./Request');
const ServerError = require('./ServerError');

class Password {
  constructor(options = {}) {
    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.model.getUser)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getAccessToken()`'
      });

    this.model = options.model;
  }

  handle(request) {
    if(!(request instanceof Request))
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: `request` must be an instance of Request'
      });

    return Promise.bind(this)
      .then(() => request.body)
      .then(({ username, password }) => this.model.getUser(username, password));
  }
}

module.exports = Password;
