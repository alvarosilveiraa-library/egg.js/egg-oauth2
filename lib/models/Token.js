'use strict';

const ServerError = require('../ServerError');

const ATTRIBUTES = [
  'accessToken',
  'accessTokenExpiresAt',
  'refreshToken',
  'refreshTokenExpiresAt',
  'scope',
  'client',
  'user'
];

class TokenModel {
  constructor(data = {}, options = {}) {
    if(!data.accessToken)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `accessToken`'
      });

    if(!data.client)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `client`'
      });

    if(!data.user)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `user`'
      });

    if(data.accessTokenExpiresAt && !(data.accessTokenExpiresAt instanceof Date))
      throw new ServerError({
        status: 500,
        message: 'Invalid parameter: `accessTokenExpiresAt`'
      });

    if(data.refreshTokenExpiresAt && !(data.refreshTokenExpiresAt instanceof Date))
      throw new ServerError({
        status: 500,
        message: 'Invalid parameter: `refreshTokenExpiresAt`'
      });

    this.accessToken = data.accessToken;

    this.accessTokenExpiresAt = data.accessTokenExpiresAt;

    this.client = data.client;

    this.refreshToken = data.refreshToken;

    this.refreshTokenExpiresAt = data.refreshTokenExpiresAt;

    this.scope = data.scope;

    this.user = data.user;

    if(options && options.allowExtendedTokenAttributes) {
      this.customAttributes = {};

      for(var key in data)
        if(data.hasOwnProperty(key) && (ATTRIBUTES.indexOf(key) < 0))
          this.customAttributes[key] = data[key];
    }

    if(this.accessTokenExpiresAt)
      this.accessTokenLifetime = Math.floor((this.accessTokenExpiresAt - new Date()) / 1000);
  }
}

module.exports = TokenModel;
