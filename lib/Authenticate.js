'use strict';

const Promise = require('bluebird');
const Request = require('./Request');
const Response = require('./Response');
const promisify = require('promisify-any').use(Promise);
const ServerError = require('./ServerError');

class Authenticate {
  constructor(options = {}) {
    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    if(!options.model.getAccessToken)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `getAccessToken()`'
      });

    if(options.scope && options.addAcceptedScopesHeader === undefined)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `addAcceptedScopesHeader`'
      });

    if(options.scope && options.addAuthorizedScopesHeader === undefined)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `addAuthorizedScopesHeader`'
      });

    if(options.scope && !options.model.verifyScope)
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: model does not implement `verifyScope()`'
      });

    this.model = options.model;

    this.scope = options.scope;

    this.addAcceptedScopesHeader = options.addAcceptedScopesHeader;

    this.addAuthorizedScopesHeader = options.addAuthorizedScopesHeader;

    this.allowBearerTokensInQueryString = options.allowBearerTokensInQueryString;
  }

  handle(request, response) {
    if(!(request instanceof Request))
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: `request` must be an instance of Request'
      });

    if(!(response instanceof Response))
      throw new ServerError({
        status: 500,
        message: 'Invalid argument: `response` must be an instance of Response'
      });

    return Promise.bind(this)
      .then(() => this.getTokenFromRequest(request))
      .then(token => this.getAccessToken(token))
      .tap(token => this.validateAccessToken(token))
      .tap(token => {
        if(!this.scope) return;

        return this.verifyScope(token);
      })
      .tap(token => this.updateResponse(response, token))
      .catch(e => {
        if(e.status === 401) response.set('WWW-Authenticate', 'Bearer realm="Service"');

        throw e;
      });
  }

  getTokenFromRequest(request) {
    const headerToken = request.get('Authorization');

    const queryToken = request.query.access_token;

    const bodyToken = request.body.access_token;

    if(!!headerToken + !!queryToken + !!bodyToken > 1)
      throw new ServerError({
        status: 500,
        message: 'Invalid request: only one authentication method is allowed'
      });

    if(headerToken) return this.getTokenFromRequestHeader(request);

    if(queryToken) return this.getTokenFromRequestQuery(request);

    if(bodyToken) return this.getTokenFromRequestBody(request);

    throw new ServerError({
      status: 401,
      message: 'Unauthorized request: no authentication given'
    });
  }

  getTokenFromRequestHeader(request) {
    const token = request.get('Authorization');

    const matches = token.match(/Bearer\s(\S+)/);

    if(!matches)
      throw new ServerError({
        status: 500,
        message: 'Invalid request: malformed authorization header'
      });

    return matches[1];
  }

  getTokenFromRequestQuery(request) {
    if(!this.allowBearerTokensInQueryString)
      throw new ServerError({
        status: 400,
        message: 'Invalid request: do not send bearer tokens in query URLs'
      });

    return request.query.access_token;
  }

  getTokenFromRequestBody(request) {
    if(request.method === 'GET')
      throw new ServerError({
        status: 400,
        message: 'Invalid request: token may not be passed in the body when using the GET verb'
      });

    if(request.is('application/x-www-form-urlencoded'))
      return Promise.reject({
        status: 400,
        message: 'Invalid request: content must be application/x-www-form-urlencoded'
      });

    return request.body.access_token;
  }

  getAccessToken(token) {
    return promisify(this.model.getAccessToken, 1).call(this.model, token)
      .then(accessToken => {
        if(!accessToken)
          throw new ServerError({
            status: 401,
            message: 'Invalid token: access token is invalid'
          });

        if(!accessToken.user)
          throw new ServerError({
            status: 503,
            message: 'Server error: `getAccessToken()` did not return a `user` object'
          });

        return accessToken;
      });
  }

  validateAccessToken(accessToken) {
    if(!(accessToken.accessTokenExpiresAt instanceof Date))
      throw new ServerError({
        status: 503,
        message: 'Server error: `accessTokenExpiresAt` must be a Date instance'
      });

    if(accessToken.accessTokenExpiresAt < new Date())
      throw new ServerError({
        status: 401,
        message: 'Invalid token: access token has expired'
      });

    return accessToken;
  }

  verifyScope(accessToken) {
    return promisify(this.model.verifyScope, 2).call(this.model, accessToken, this.scope)
      .then(scope => {
        if(!scope)
          throw new ServerError({
            status: 403,
            message: 'Insufficient scope: authorized scope is insufficient'
          });

        return scope;
      });
  }

  updateResponse(response, accessToken) {
    if(this.scope && this.addAcceptedScopesHeader)
      response.set('X-Accepted-OAuth-Scopes', this.scope);

    if(this.scope && this.addAuthorizedScopesHeader)
      response.set('X-OAuth-Scopes', accessToken.scope);
  }
}

module.exports = Authenticate;
