'use strict';

const crypto = require('crypto');
const randomBytes = require('bluebird').promisify(crypto.randomBytes);

exports.is = {
  nchar: value => /^[\u002D|\u002E|\u005F|\w]+$/.test(value),
  nqchar: value => /^[\u0021|\u0023-\u005B|\u005D-\u007E]+$/.test(value),
  nqschar: value => /^[\u0020-\u0021|\u0023-\u005B|\u005D-\u007E]+$/.test(value),
  uchar: value => /^[\u0009|\u0020-\u007E|\u0080-\uD7FF|\uE000-\uFFFD|\u10000-\u10FFFF]+$/.test(value),
  uri: value => /^[a-zA-Z][a-zA-Z0-9+.-]+:/.test(value),
  vschar: value => /^[\u0020-\u007E]+$/.test(value)
};

exports.generateRandomToken = () => {
  return randomBytes(256).then(function(buffer) {
    return crypto
      .createHash('sha1')
      .update(buffer)
      .digest('hex');
  });
};

exports.replaceResponse = res => {
  const response = {
    headers: {}
  };

  Object.keys(res).forEach(key => {
    if(key !== 'headers')
      response[key] = res[key];
  });

  Object.keys(res.headers).forEach(key => {
    response.headers[key] = res.headers[key];
  });

  response.header = response.headers;

  return response;
};

exports.handleResponse = (ctx, response) => {
  ctx.set(response.headers);

  ctx.status = response.status;

  ctx.body = response.body;
};

exports.handleResponseExpress = (res, response) => {
  res.set(response.headers);

  res
    .status(response.status)
    .json(response.body);
};

exports.handleError = (ctx, response, e) => {
  if(response) ctx.set(response.headers);

  throw e;
};

exports.handleErrorExpress = (res, response, e) => {
  if(response) res.set(response.headers);

  throw e;
};
