'use strict';

const _ = require('lodash');
const Authenticate = require('./Authenticate');
const Authorize = require('./Authorize');
const Token = require('./Token');
const ServerError = require('./ServerError');

class Server {
  constructor(options = {}) {
    if(!options.model)
      throw new ServerError({
        status: 500,
        message: 'Missing parameter: `model`'
      });

    this.options = options;
  }

  authenticate(request, response, options, callback) {
    if(typeof options === 'string') options = { scope: options };

    options = _.assign({
      addAcceptedScopesHeader: true,
      addAuthorizedScopesHeader: true,
      allowBearerTokensInQueryString: false
    }, this.options, options);

    return new Authenticate(options)
      .handle(request, response)
      .nodeify(callback);
  }

  authorize(request, response, options, callback) {
    options = _.assign({
      allowEmptyState: false,
      authorizationCodeLifetime: 5 * 60
    }, this.options, options);

    return new Authorize(options)
      .handle(request, response)
      .nodeify(callback);
  }

  token(request, response, options, callback) {
    options = _.assign({
      accessTokenLifetime: 60 * 60,
      refreshTokenLifetime: 60 * 60 * 24 * 14,
      allowExtendedTokenAttributes: false,
      requireClientAuthentication: {}
    }, this.options, options);

    return new Token(options)
      .handle(request, response)
      .nodeify(callback);
  }
}

module.exports = Server;
